The iPython files provided in this repository are intended to be viewed 
through iPython notebooks (Jupyter notebooks). While students can certainly
install iPython on their computers, a much simpler way is to use the
uWaterloo Jupyter hub, which avoids the need for manual installation.

The server can be access at https://jupyter.math.uwaterloo.ca with your uWaterloo
log-in credentials (the same ones that would be used for Quest or Learn).
